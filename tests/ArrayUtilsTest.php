<?php
	declare(strict_types=1);

	use com\femastudios\utils\core\ArrayUtils;
	use PHPUnit\Framework\TestCase;

	class ArrayUtilsTest extends TestCase {

		public function testRemoveDuplicates() : void {
			static::assertSame(['a', 'b', 'c'], ArrayUtils::removeDuplicates(['a', 'b', 'b', 'c', 'a']));
		}

		public function testContainsAll() : void {
			static::assertTrue(ArrayUtils::containsAll(['a', 'b', 'c'], ['a', 'c']));
			static::assertFalse(ArrayUtils::containsAll(['a', 'b', 'c'], ['a', 'd']));
			static::assertTrue(ArrayUtils::containsAll(['1', '2', '3'], [true, 3], false));
			static::assertFalse(ArrayUtils::containsAll(['1', '2', '3'], [true, 3, 5.5], false));
		}

		public function testSameValues() : void {
			static::assertTrue(ArrayUtils::sameValues([1, 2, 3], [2, 3, 1]));
			static::assertTrue(ArrayUtils::sameValues(['1', '2', 3], ['2', '3', 1], false));
			static::assertFalse(ArrayUtils::sameValues([1, 2, 3], [2, 3, 1, 1]));
		}

		public function testSameSet() : void {
			static::assertTrue(ArrayUtils::sameSet([1, 2, 3], [2, 3, 1]));
			static::assertTrue(ArrayUtils::sameSet([1, 2, 2, 1, 3], [2, 3, 1, 1, 3, 2]));
			static::assertTrue(ArrayUtils::sameSet(['1', '2', 3], ['2', '3', 1], false));
			static::assertFalse(ArrayUtils::sameSet([1, 2, 3], [2, 3, 1, 1, 5, 6, 7]));
		}

		public function testDeepContains() : void {
			static::assertFalse(ArrayUtils::deepContains(['a', 'b', 'c'], ['a' => []]));
			static::assertFalse(ArrayUtils::deepContains(['a', 'b', 'c'], ['a' => ['b' => 'not an array']]));
			static::assertTrue(ArrayUtils::deepContains(['a', 'b', 'c'], ['z' => '', 'a' => ['b' => ['c' => 'hello']]]));
		}

		public function testDeepGetWrong1() : void {
			$this->expectException(\Exception::class);
			ArrayUtils::deepGet(['a', 'b', 'c'], ['a' => []]);
		}

		public function testDeepGetWrong2() : void {
			$this->expectException(\Exception::class);
			ArrayUtils::deepGet(['a', 'b', 'c'], ['a' => ['b' => 'not an array']]);
		}

		public function testDeepGet() : void {
			static::assertSame('hello', ArrayUtils::deepGet(['a', 'b', 'c'], ['z' => '', 'a' => ['b' => ['c' => 'hello']]]));
		}

		public function testMergeWithoutDuplicates() : void {
			static::assertSame(['a', 'b', 'c'], ArrayUtils::mergeWithoutDuplicates(['a', 'a', 'b'], ['b', 'b', 'b', 'c', 'c']));
		}

		public function testPushAll() : void {
			$array = [1, 2, 3, 4];
			ArrayUtils::pushAll($array, [5, 6, 7, 8]);
			static::assertSame([1, 2, 3, 4, 5, 6, 7, 8], $array);
		}

		public function testDiff() : void {
			static::assertSame([1, 3], ArrayUtils::diff([1, 2, 3, 4], [2, 4]));
			static::assertSame([1, '3'], ArrayUtils::diff([1, 2, '3', 4], [2, '4'], false));
		}

		public function testCopy() : void {
			static::assertSame([1, 2, 3], ArrayUtils::copy([1, 2, 3]));
		}

		public function testEquals() : void {
			static::assertTrue(ArrayUtils::equals([1, 2, 3], [1, 2, 3]));
			static::assertTrue(ArrayUtils::equals([1, '2', 3], ['1', 2, 3], false));
		}

		public function testMissingKeys() : void {
			static::assertSame(['c'], ArrayUtils::missingKeys(['a' => '', 'b' => '', 'd' => ''], ['b', 'c', 'd']));
			static::assertSame([], ArrayUtils::missingKeys(['a' => '', 'b' => '', 'c' => ''], ['a', 'b', 'c']));
		}

		public function testDeepMerge() : void {
			static::assertSame([1 => '', 2 => '', 4 => [5 => '', 6 => '', 7 => '', 8 => ''], 3 => ''], ArrayUtils::deepMerge([1 => '', 2 => '', 4 => [5 => '', 6 => '']], [3 => '', 4 => [7 => '', 8 => '']]));
		}

		public function testFilterNull() : void {
			static::assertSame([1, 2, 0, 4, 5], ArrayUtils::filterNull([1, 2, null, 4, 5], 0));
			static::assertSame([0 => 1, 1 => 2, 3 => 4, 4 => 5], ArrayUtils::filterNull([1, 2, null, 4, 5], null));
			static::assertSame([0 => 1, 1 => 2, 3 => 4, 4 => 5], ArrayUtils::filterNull([1, 2, null, 4, 5]));
		}
	}
