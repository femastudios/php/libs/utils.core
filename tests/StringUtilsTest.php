<?php
	declare(strict_types=1);

	use com\femastudios\utils\core\StringUtils;
	use PHPUnit\Framework\TestCase;

	class StringUtilsTest extends TestCase {


		public function testStartsEndsWith() : void {
			static::assertTrue(StringUtils::startsWith('hello', 'hello how are you'));
			static::assertFalse(StringUtils::startsWith('hi', 'hello how are you'));

			static::assertTrue(StringUtils::endsWith('you', 'hello how are you'));
			static::assertFalse(StringUtils::endsWith('are', 'hello how are you'));
		}

		public function testReverse() : void {
			static::assertSame('olleh', StringUtils::reverse('hello'));
			static::assertSame('anna', StringUtils::reverse('anna'));
		}

		public function testGetRepeatedCharsCount() : void {
			static::assertSame(9, StringUtils::getRepeatedCharsCount('helloaa a bbb cccccc', 3));
			static::assertSame(6, StringUtils::getRepeatedCharsCount('helloaa a bbb cccccc', 4));
			static::assertSame(6, StringUtils::getRepeatedCharsCount('helloaa a bbB cccCcc', 4, true));
			static::assertSame(3, StringUtils::getRepeatedCharsCount('helloaa a bbB cccCcc', 3, false));
		}

		public function testReplace() : void {
			static::assertSame("it's sunny outside; it's also very hot", StringUtils::replace('it is', "it's", 'it is sunny outside; it is also very hot'));
			$cnt = 0;
			static::assertSame('good goodder buddy goodminton', StringUtils::replace('bad', 'good', 'bad badder buddy badminton', $cnt));
			static::assertSame(3, $cnt);

			static::assertSame('hello', StringUtils::replace('', 'a', 'hello', $cnt));
			static::assertSame(0, $cnt);
		}

		public function testNormalizeNewlines() : void {
			static::assertSame("win->\n\nmac->\n\nunix->\n\n", StringUtils::normalizeNewlines("win->\r\n\r\nmac->\r\runix->\n\n"));
		}

		public function testVersionCompare() : void {
			static::assertGreaterThan(0, StringUtils::versionCompare('1.0', '0.9'));
			static::assertGreaterThan(0, StringUtils::versionCompare('1', '0.9'));
			static::assertSame(0, StringUtils::versionCompare('1.0', '1.0'));
			static::assertLessThan(0, StringUtils::versionCompare('1.0', '1.0a'));
			static::assertLessThan(0, StringUtils::versionCompare('1.0', '1.0b'));
			static::assertLessThan(0, StringUtils::versionCompare('1.0', '1.0ab'));
			static::assertGreaterThan(0, StringUtils::versionCompare('1.0ab', '1.0aa'));
			static::assertGreaterThan(0, StringUtils::versionCompare('1.0aa.4', '1.0aa'));
			static::assertLessThan(0, StringUtils::versionCompare('1.t.1', '1.tt'));
		}

		public function testChars() : void {
			static::assertSame(['h', 'e', 'l', 'l', 'o'], StringUtils::chars('hello'));
			static::assertSame(['💩', '💩', '💩'], StringUtils::chars('💩💩💩'));
		}

		public function testParseIntOrNull() : void {
			static::assertNull(StringUtils::parseIntOrNull('hello'));
			static::assertNull(StringUtils::parseIntOrNull(' 123'));
			static::assertNull(StringUtils::parseIntOrNull('123 '));
			static::assertSame(123, StringUtils::parseIntOrNull('123'));
			static::assertSame(123, StringUtils::parseIntOrNull('+123'));
			static::assertSame(-123, StringUtils::parseIntOrNull('-123'));
		}

		public function testParseInt() : void {
			static::assertSame(123, StringUtils::parseIntOrNull('123'));
			$this->expectException(\DomainException::class);
			StringUtils::parseInt(' 12');
		}
	}
