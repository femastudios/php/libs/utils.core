<?php
	declare(strict_types=1);

	use com\femastudios\utils\core\intSet\IntRange;
	use PHPUnit\Framework\TestCase;

	class IntRangeTest extends TestCase {

		public function testIncInc() : void {
			$range = IntRange::finite(1, 3, true, true);
			static::assertFalse($range->isContained(0));
			static::assertTrue($range->isContained(1));
			static::assertTrue($range->isContained(2));
			static::assertTrue($range->isContained(3));
			static::assertFalse($range->isContained(4));
		}

		public function testIncExc() : void {
			$range = IntRange::finite(1, 3, true, false);
			static::assertFalse($range->isContained(0));
			static::assertTrue($range->isContained(1));
			static::assertTrue($range->isContained(2));
			static::assertFalse($range->isContained(3));
			static::assertFalse($range->isContained(4));
		}

		public function testExcInc() : void {
			$range = IntRange::finite(1, 3, false, true);
			static::assertFalse($range->isContained(0));
			static::assertFalse($range->isContained(1));
			static::assertTrue($range->isContained(2));
			static::assertTrue($range->isContained(3));
			static::assertFalse($range->isContained(4));
		}

		public function testExcExc() : void {
			$range = IntRange::finite(1, 3, false, false);
			static::assertFalse($range->isContained(0));
			static::assertFalse($range->isContained(1));
			static::assertTrue($range->isContained(2));
			static::assertFalse($range->isContained(3));
			static::assertFalse($range->isContained(4));
		}


		public function testToString() : void {
			static::assertSame('[1, 5)', (string)IntRange::finite(1, 5, true, false));
		}

		public function testStartEndBackwards() : void {
			$this->expectException(\RangeException::class);
			IntRange::finite(5, 1);
		}

		public function testSameNumberIncMismatch() : void {
			$this->expectException(\LogicException::class);
			IntRange::finite(1, 1, true, false);
		}


		public function testCompareStarts() : void {
			static::assertLessThan(0, IntRange::compareStarts(IntRange::finite(1, 2), IntRange::finite(2, 4)));
			static::assertLessThan(0, IntRange::compareStarts(IntRange::startOpen(2), IntRange::finite(2, 4)));
			static::assertLessThan(0, IntRange::compareStarts(IntRange::finite(1, 2, true), IntRange::finite(1, 4, false)));

			static::assertEquals(0, IntRange::compareStarts(IntRange::finite(2, 2, true), IntRange::finite(1, 4, false)));
			static::assertEquals(0, IntRange::compareStarts(IntRange::startOpen(2), IntRange::startOpen(2)));
		}

		public function testCompareEnds() : void {
			static::assertLessThan(0, IntRange::compareEnds(IntRange::finite(1, 2), IntRange::finite(2, 4)));
			static::assertLessThan(0, IntRange::compareEnds(IntRange::finite(1, 2, true, true), IntRange::finite(1, 4, false, false)));

			static::assertEquals(0, IntRange::compareEnds(IntRange::startOpen(2), IntRange::startOpen(2)));
			static::assertEquals(0, IntRange::compareEnds(IntRange::finite(1, 3, true, true), IntRange::finite(1, 4, false, false)));

			static::assertGreaterThan(0, IntRange::compareEnds(IntRange::endOpen(2), IntRange::finite(2, 4)));
			static::assertGreaterThan(0, IntRange::compareEnds(IntRange::finite(1, 4, true, true), IntRange::finite(1, 4, false, false)));
		}

		public function testCardinality() : void {
			static::assertEquals(null, IntRange::compareEnds(IntRange::startOpen(2), IntRange::finite(1, 2)));
			static::assertEquals(3, IntRange::finite(1, 3, true, true)->getCardinality());
			static::assertEquals(2, IntRange::finite(1, 3, true, false)->getCardinality());
			static::assertEquals(2, IntRange::finite(1, 3, false, true)->getCardinality());
			static::assertEquals(1, IntRange::finite(1, 3, false, false)->getCardinality());
		}

		public function testEquals() : void {
			static::assertTrue(IntRange::startOpen(20, true)->equals(IntRange::startOpen(21, false)));
			static::assertTrue(IntRange::finite(10, 40, true, true)->equals(IntRange::finite(9, 41, false, false)));
			static::assertFalse(IntRange::finite(5, 60)->equals(IntRange::finite(1, 100)));
		}

		public function testIntersection() : void {
			static::assertEquals(IntRange::finite(20, 24), IntRange::finite(18, 66)->intersect(IntRange::finite(20, 24)));
			static::assertEquals(IntRange::finite(10, 20), IntRange::startOpen(20)->intersect(IntRange::finite(10, 50)));
			static::assertEquals(IntRange::startOpen(20), IntRange::startOpen(20)->intersect(IntRange::startOpen(50)));

			static::assertNotEquals(IntRange::startOpen(50), IntRange::startOpen(20)->intersect(IntRange::startOpen(50)));
		}

		public function testOverlaps() : void {
			static::assertTrue(IntRange::finite(10, 20)->overlaps(IntRange::finite(15, 25)));
			static::assertFalse(IntRange::finite(10, 20, true, false)->overlaps(IntRange::finite(19, 25, false, true)));
			static::assertTrue(IntRange::startOpen(50)->overlaps(IntRange::endOpen(50)));
		}

		public function testCombine() : void {
			static::assertTrue(IntRange::finite(10, 20)->combine(IntRange::finite(15, 50))->equals(IntRange::finite(10, 50)));
			static::assertTrue(IntRange::startOpen(10)->combine(IntRange::finite(5, 50))->equals(IntRange::startOpen(50)));
			static::assertTrue(IntRange::startOpen(10)->combine(IntRange::endOpen(5))->equals(IntRange::allIntegers()));
			static::assertTrue(IntRange::finite(10, 20)->combine(IntRange::finite(21, 30))->equals(IntRange::finite(10, 30)));
		}

		public function testAdjacency() : void {
			//Left
			static::assertTrue(IntRange::finite(1, 10)->isLeftAdjacentOf(IntRange::finite(11, 20)));
			static::assertFalse(IntRange::finite(11, 20)->isLeftAdjacentOf(IntRange::finite(1, 10)));
			static::assertFalse(IntRange::finite(1, 10)->isLeftAdjacentOf(IntRange::finite(10, 20)));

			//Right
			static::assertFalse(IntRange::finite(1, 10)->isRightAdjacentOf(IntRange::finite(11, 20)));
			static::assertTrue(IntRange::finite(11, 20)->isRightAdjacentOf(IntRange::finite(1, 10)));
			static::assertFalse(IntRange::finite(1, 10)->isRightAdjacentOf(IntRange::finite(10, 20)));

			//Both
			static::assertTrue(IntRange::finite(1, 10)->isAdjacent(IntRange::finite(11, 20)));
			static::assertTrue(IntRange::finite(11, 20)->isAdjacent(IntRange::finite(1, 10)));
			static::assertFalse(IntRange::finite(1, 10)->isAdjacent(IntRange::finite(10, 20)));
		}

		public function testCombineException() : void {
			$this->expectException(\DomainException::class);
			IntRange::finite(10, 20)->combine(IntRange::finite(30, 50));
		}

		public function testIsSubrangeOf() : void {
			static::assertTrue(IntRange::finite(1, 100)->isSubRangeOf(IntRange::endOpen(1)));
			static::assertFalse(IntRange::finite(1, 100)->isSubRangeOf(IntRange::finite(5, 60)));
			static::assertFalse(IntRange::finite(1, 100)->isSubRangeOf(IntRange::finite(10000, 50000)));
		}

		public function testAccessoryMethods() : void {
			static::assertTrue(IntRange::finite(1, 2, true, false)->isSingle());
		}

		public function testIterator() : void {
			static::assertSame([1, 2, 3, 4, 5], iterator_to_array(IntRange::finite(1, 5)));
			static::assertSame([70, 71, 72], iterator_to_array(IntRange::finite(70, 73, true, false)));
			static::assertSame([71, 72, 73], iterator_to_array(IntRange::finite(70, 73, false, true)));
			static::assertSame([71, 72], iterator_to_array(IntRange::finite(70, 73, false, false)));
		}

		public function testIteratorInfinite() : void {
			$this->expectException(\LogicException::class);
			IntRange::startOpen(50)->getIterator();
		}

		public function testString() : void {
			self::assertSame('2010-2020', IntRange::finite(2010, 2020)->string());
			self::assertSame('2010-2020', IntRange::finite(2009, 2020, false)->string());
			self::assertSame('2010-2020', IntRange::finite(2009, 2021, false, false)->string());

			self::assertSame('-10-10', IntRange::finite(-10, 10)->string());
			self::assertSame('-20--10', IntRange::finite(-20, -10)->string());

			self::assertSame('2010-*', IntRange::endOpen(2009, false)->string());
			self::assertSame('2010-*', IntRange::endOpen(2010)->string());

			self::assertSame('*-2010', IntRange::startOpen(2011, false)->string());
			self::assertSame('*-2010', IntRange::startOpen(2010)->string());

			self::assertSame('*', IntRange::allIntegers()->string());
			self::assertSame('2010', IntRange::single(2010)->string());
		}


		public function testParseString() : void {
			self::assertEquals(IntRange::finite(2010, 2020), IntRange::parseString('2010-2020'));
			self::assertEquals(IntRange::startOpen(2020), IntRange::parseString('*-2020'));
			self::assertEquals(IntRange::endOpen(2020), IntRange::parseString('2020-*'));
			self::assertEquals(IntRange::allIntegers(), IntRange::parseString('*'));

			self::assertEquals(IntRange::finite(-10, -5), IntRange::parseString('-10--5'));
			self::assertEquals(IntRange::finite(-10, 5), IntRange::parseString('-10-5'));
		}
	}

