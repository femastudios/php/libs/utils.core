<?php
	declare(strict_types=1);

	use com\femastudios\utils\core\MathUtils;
	use PHPUnit\Framework\TestCase;

	class MathUtilsTest extends TestCase {


		public function testConstrain() : void {
			/* INTS */

			//Positive test case
			static::assertSame(10,MathUtils::constrainInt(10,1,20));
			static::assertSame(1,MathUtils::constrainInt(-10,1,20));
			static::assertSame(20,MathUtils::constrainInt(+40,1,20));

			//Negative test case
			static::assertSame(-10,MathUtils::constrainInt(-10,-20,-1));
			static::assertSame(-20,MathUtils::constrainInt(-30,-20,-1));
			static::assertSame(-1,MathUtils::constrainInt(0,-20,-1));


			/* FLOATS */
			//Positive test case
			static::assertSame(10.4, MathUtils::constrainFloat(10.4,1.0,20.0));
			static::assertSame(1.0, MathUtils::constrainFloat(-10.5,1.0,20.0));
			static::assertSame(20.0, MathUtils::constrainFloat(+40.45,1.0,20.0));

			//Negative test case
			static::assertSame(-10.12, MathUtils::constrainFloat(-10.12,-20.0,-1.0));
			static::assertSame(-20.0, MathUtils::constrainFloat(-30.0,-20.0,-1.0));
			static::assertSame(-1.0, MathUtils::constrainFloat(0.0,-20.0,-1.0));
		}

		public function testCompare() : void {
			/* INTS */
			static::assertSame(0, MathUtils::compareInt(1,1));
			static::assertSame(-1, MathUtils::compareInt(1,2));
			static::assertSame(+1, MathUtils::compareInt(2,1));

			/* FLOATS */
			static::assertSame(0, MathUtils::compareFloat(1.0,1.0));
			static::assertSame(-1, MathUtils::compareFloat(1.0,2.0));
			static::assertSame(+1, MathUtils::compareFloat(2.0,1.0));
		}
	}
