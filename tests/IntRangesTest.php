<?php
	declare(strict_types=1);

	use com\femastudios\utils\core\intSet\IntRange;
	use com\femastudios\utils\core\intSet\IntRanges;
	use PHPUnit\Framework\TestCase;

	class IntRangesTest extends TestCase {

		public function testBase() : void {
			$range1 = IntRange::finite(1, 3, true, true);
			$range2 = IntRange::finite(7, 10, false, true);
			$range3 = IntRange::finite(8, 15, true, false);
			$ranges = IntRanges::ranges($range1, $range2, $range3);

			static::assertFalse($ranges->isContained(0));
			static::assertTrue($ranges->isContained(1));
			static::assertTrue($ranges->isContained(2));
			static::assertTrue($ranges->isContained(3));
			static::assertFalse($ranges->isContained(4));
			static::assertFalse($ranges->isContained(5));
			static::assertFalse($ranges->isContained(6));
			static::assertFalse($ranges->isContained(7));
			static::assertTrue($ranges->isContained(8));
			static::assertTrue($ranges->isContained(9));
			static::assertTrue($ranges->isContained(10));
			static::assertTrue($ranges->isContained(11));
			static::assertTrue($ranges->isContained(12));
			static::assertTrue($ranges->isContained(13));
			static::assertTrue($ranges->isContained(14));
			static::assertFalse($ranges->isContained(15));
			static::assertFalse($ranges->isContained(16));
		}

		public function testSimplified() : void {
			$simplified = IntRanges::ranges(IntRange::finite(1, 40), IntRange::finite(70, 80), IntRange::finite(20, 50))->simplified();
			$expected = IntRanges::ranges(IntRange::finite(1, 50), IntRange::finite(70, 80));

			static::assertTrue($simplified->equals($expected));
		}

		public function testIterator() : void {
			static::assertSame([1, 2, 3, 4, 5, 6, 7, 8, 50, 51, 52], iterator_to_array(IntRanges::ranges(IntRange::finite(1, 6), IntRange::finite(4, 8), IntRange::finite(50, 52))));
		}

		public function testIteratorInfinite() : void {
			$this->expectException(\LogicException::class);
			IntRanges::ranges(IntRange::endOpen(50))->getIterator();
		}

		public function testString() : void {
			self::assertSame('2010,2015-2020', IntRanges::ranges(IntRange::single(2010), IntRange::finite(2015, 2020))->string());
			self::assertSame('2010,2015-*', IntRanges::ranges(IntRange::single(2010), IntRange::endOpen(2015))->string());
			self::assertSame('*-2010,2015', IntRanges::ranges(IntRange::startOpen(2010), IntRange::single(2015))->string());
			self::assertSame('*', IntRanges::ranges(IntRange::allIntegers())->string());
		}

		public function testParseString() : void {
			self::assertEquals(IntRanges::ranges(IntRange::single(2010), IntRange::finite(2015, 2020)), IntRanges::parseString('2010,2015-2020'));
			self::assertEquals(IntRanges::ranges(IntRange::single(2010), IntRange::endOpen(2015)), IntRanges::parseString('2010,2015-*'));
			self::assertEquals(IntRanges::ranges(IntRange::startOpen(2010), IntRange::single(2015)), IntRanges::parseString('*-2010,2015'));
			self::assertEquals(IntRanges::ranges(IntRange::allIntegers()), IntRanges::parseString('*'));
		}
	}
