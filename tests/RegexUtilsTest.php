<?php
	declare(strict_types=1);

	use com\femastudios\utils\core\RegexUtils;
	use PHPUnit\Framework\TestCase;

	class RegexUtilsTest extends TestCase {

		public function testGetIntRangeRegexWrongRange() : void {
			$this->expectException(\DomainException::class);
			RegexUtils::getIntRangeRegex(1, 0);
		}

		private function assertMatchesAll(string $pattern, string ...$matches) : void {
			foreach ($matches as $match) {
				static::assertSame(preg_match($pattern, $match), 1);
			}
		}

		private function assertNotMatchesAll(string $pattern, string ...$matches) : void {
			foreach ($matches as $match) {
				static::assertSame(preg_match($pattern, $match), 0);
			}
		}

		public function testGetIntRangeRegex() : void {
			//ZERO ZERO
			$this->assertMatchesAll('/^' . RegexUtils::getIntRangeRegex(0, 0, false) . '$/', '0', '00000', '+00000000');
			$this->assertNotMatchesAll('/^' . RegexUtils::getIntRangeRegex(0, 0, false) . '$/', '-0', '1', '+1', '-1', '-000001', '0001', '+0001');

			//ZERO POS
			$this->assertMatchesAll('/^' . RegexUtils::getIntRangeRegex(0, 10) . '$/', '5', '2', '10', '+7', '0', '-0', '1');
			$this->assertNotMatchesAll('/^' . RegexUtils::getIntRangeRegex(0, 10) . '$/', '100', '-10', '-1', '-50', '+100');

			//NEG ZERO
			$this->assertMatchesAll('/^' . RegexUtils::getIntRangeRegex(-10, 0) . '$/', '0', '-0', '000', '-00', '-010', '-1', '-5');
			$this->assertNotMatchesAll('/^' . RegexUtils::getIntRangeRegex(-10, 0) . '$/', '+555', '00055', '-100', '100');

			//POS POS
			$this->assertMatchesAll('/^' . RegexUtils::getIntRangeRegex(2, 10) . '$/', '5', '2', '10', '+7');
			$this->assertNotMatchesAll('/^' . RegexUtils::getIntRangeRegex(2, 10) . '$/', '100', '-10', '1', '-50', '-0', '0', '+100');

			//NEG NEG
			$this->assertMatchesAll('/^' . RegexUtils::getIntRangeRegex(-200, -20) . '$/', '-000020', '-200', '-40', '-150');
			$this->assertNotMatchesAll('/^' . RegexUtils::getIntRangeRegex(-200, -20) . '$/', '+80', '-0', '0', '-2000', '-2', '500');

			//NEG POS
			$this->assertMatchesAll('/^' . RegexUtils::getIntRangeRegex(-4, +5) . '$/', '+0', '0', '-0000', '-004', '-1', '005', '+5', '+1', '-4');
			$this->assertNotMatchesAll('/^' . RegexUtils::getIntRangeRegex(-4, +5) . '$/', '-40', '+50', '100');
		}
	}