<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	use PHPUnit\Framework\TestCase;

	class GreyListTest extends TestCase {

		public function test1() : void {
			$in = ['ciao', 1, 55];
			$lst = GreyList::newWhitelist();
			$lst->add(...$in);

			self::assertSame($in, $lst->filter([]));

			$lst->becomeBlacklist(false);
			self::assertSame([22, 33, 45, 65], array_values($lst->filter([1, 22, 33, 45, 65, 55])));
		}

	}
