<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	final class IntUtils {

		private function __construct() {
		}

		/**
		 * @param int $min min value
		 * @param int $max max value
		 * @param bool $needsEntropy if a cryptographically secure random is required. Defaults to false
		 * @return int the random int
		 * @throws \Exception if <code>needsEntropy</code> is true and <code>random_int()</code> throws for insufficient entropy
		 */
		public static function randomInt(int $min, int $max, bool $needsEntropy = false) : int {
			try {
				return \random_int($min, $max);
			} catch (\Exception $e) {
				if($needsEntropy) {
					throw $e;
				} else {
					/** @noinspection RandomApiMigrationInspection */
					return \mt_rand($min, $max);
				}
			}
		}

	}