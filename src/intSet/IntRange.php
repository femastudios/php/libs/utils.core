<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core\intSet;

	use com\femastudios\utils\core\MathUtils;
	use com\femastudios\utils\core\ObjectsUtils;
	use com\femastudios\utils\core\StringUtils;

	final class IntRange extends IntSet {

		private ?int $start;
		private ?int $end;
		private bool $startInclusive;
		private bool $endInclusive;

		private function __construct(?int $start, ?int $end, bool $startInclusive = true, bool $endInclusive = true) {
			if ($start !== null && $end !== null) {
				if ($end < $start) {
					throw new \RangeException('start < end');
				} elseif ($start === $end && (!$startInclusive || !$endInclusive)) {
					throw new \LogicException('when start and end are equals, both startInclusive and endInclusive must be true');
				} elseif ($start === $end - 1 && !$startInclusive && !$endInclusive) {
					throw new \LogicException('Cannot create a range that accepts no integers. Use EmptyIntSet');
				}
			} else {
				if ($start === null && $startInclusive) {
					throw new \LogicException('Cannot have an inclusive open start');
				}
				if ($end === null && $endInclusive) {
					throw new \LogicException('Cannot have an inclusive open end');
				}
			}
			$this->start = $start;
			$this->end = $end;
			$this->startInclusive = $this->start === null ? false : $startInclusive;
			$this->endInclusive = $this->end === null ? false : $endInclusive;
		}

		/**
		 * @return int|null the start value, null for -infinity.
		 */
		public function getStart() : ?int {
			return $this->start;
		}

		/**
		 * @return int|null the end value, null for +infinity
		 */
		public function getEnd() : ?int {
			return $this->end;
		}

		/**
		 * @return int the finite start
		 * @throws \LogicException if start is -infinity
		 */
		public function getFiniteStart() : int {
			if ($this->isStartInfinite()) {
				throw new \LogicException('start is -infinity');
			}
			return $this->start;
		}

		/**
		 * @return int the finite end
		 * @throws \LogicException if end is +infinity
		 */
		public function getFiniteEnd() : int {
			if ($this->isEndInfinite()) {
				throw new \LogicException('end is +infinity');
			}
			return $this->end;
		}

		/**
		 * @return bool true if start is -infinity
		 */
		public function isStartInfinite() : bool {
			return $this->start === null;
		}

		/**
		 * @return bool true if start is a number
		 */
		public function isStartFinite() : bool {
			return $this->start !== null;
		}

		/**
		 * @return bool true if start is inclusive. Always false for -infinity
		 */
		public function isStartInclusive() : bool {
			return $this->startInclusive;
		}

		/**
		 * @return bool true if start is exclusive. Always true for -infinity
		 */
		public function isStartExclusive() : bool {
			return !$this->startInclusive;
		}

		/**
		 * @return bool true if end is +infinity
		 */
		public function isEndInfinite() : bool {
			return $this->end === null;
		}

		/**
		 * @return bool true if end is a number
		 */
		public function isEndFinite() : bool {
			return $this->end !== null;
		}

		/**
		 * @return bool true if end is inclusive. Always false for +infinity
		 */
		public function isEndInclusive() : bool {
			return $this->endInclusive;
		}

		/**
		 * @return bool true if end is exclusive. Always true for +infinity
		 */
		public function isEndExclusive() : bool {
			return !$this->endInclusive;
		}

		/**
		 * @return bool true if at start OR end are infinite
		 */
		public function isInfinite() : bool {
			return $this->isStartInfinite() || $this->isEndInfinite();
		}

		/**
		 * @return bool true if the range represents a single number (e.g. [1,1] or [1,2))
		 */
		public function isSingle() : bool {
			return $this->isFinite() && $this->getInclusiveStart() === $this->getInclusiveEnd();
		}

		/**
		 * @return bool true if the range represents all possible integers (-inf, +inf)
		 */
		public function isAllIntegers() : bool {
			return $this->isInfinite() && $this->start === $this->end;
		}

		/**
		 * {@inheritDoc}
		 */
		public function getCardinality() : ?int {
			if ($this->isFinite()) {
				$ret = $this->end - $this->start - 1;
				if ($this->startInclusive) {
					$ret++;
				}
				if ($this->endInclusive) {
					$ret++;
				}
				return $ret;
			} else {
				return null;
			}
		}

		/**
		 * Given two IntRange(s) compares the start values
		 * @param IntRange $range1 the first IntRange
		 * @param IntRange $range2 the second IntRange
		 * @return int -1, 0 or +1 if the first range's start is less than, equals to, or greater than the second range, respectively
		 */
		public static function compareStarts(IntRange $range1, IntRange $range2) : int {
			$start1Infinite = $range1->isStartInfinite();
			$start2Infinite = $range2->isStartInfinite();
			if ($start1Infinite && $start2Infinite) {
				return 0;
			} elseif ($start1Infinite && !$start2Infinite) {
				return -1;
			} elseif (!$start1Infinite && $start2Infinite) {
				return +1;
			} elseif (!$start1Infinite && !$start2Infinite) {
				return MathUtils::compareInt($range1->getInclusiveStart(), $range2->getInclusiveStart());
			} else {
				throw new \LogicException('Illegal state');
			}
		}

		/**
		 * Given two IntRange(s) compares the end values
		 * @param IntRange $range1 the first IntRange
		 * @param IntRange $range2 the second IntRange
		 * @return int -1, 0 or +1 if the first range's end is less than, equals to, or greater than the second range, respectively
		 */

		public static function compareEnds(IntRange $range1, IntRange $range2) : int {
			$end1Infinite = $range1->isEndInfinite();
			$end2Infinite = $range2->isEndInfinite();
			if ($end1Infinite && $end2Infinite) {
				return 0;
			} elseif ($end1Infinite && !$end2Infinite) {
				return +1;
			} elseif (!$end1Infinite && $end2Infinite) {
				return -1;
			} elseif (!$end1Infinite && !$end2Infinite) {
				return MathUtils::compareInt($range1->getInclusiveEnd(), $range2->getInclusiveEnd());
			} else {
				throw new \LogicException('Illegal state');
			}
		}

		/**
		 * {@inheritDoc}
		 */
		public function equals(IntSet $another) : bool {
			if ($another instanceof self) {
				return IntRange::compareStarts($this, $another) === 0 && IntRange::compareEnds($this, $another) === 0;
			} elseif ($another instanceof IntRanges) {
				$ranges = $another->simplified()->getRanges();
				if (\count($ranges) === 1) {
					return $this->equals($ranges[0]);
				} else {
					return false;
				}
			} else {
				throw new \InvalidArgumentException('Unsupported IntSet of type ' . \get_class($another));
			}
		}

		/**
		 * Intersects this IntRange to another one
		 * @param IntRange $another the other IntRange
		 * @return IntRange|null the intersection result, or null if the intersection is empty
		 */
		public function intersect(IntRange $another) : ?IntRange {
			//Get maximum of low ends
			if (IntRange::compareStarts($this, $another) >= 0) {
				$maxStart = $this;
			} else {
				$maxStart = $another;
			}
			//Get minimum of high ends
			if (IntRange::compareEnds($this, $another) <= 0) {
				$minEnd = $this;
			} else {
				$minEnd = $another;
			}
			if ($minEnd === $maxStart) {
				return $minEnd;
			} elseif ($maxStart->isStartFinite() && $minEnd->isEndFinite() && $maxStart->getInclusiveStart() > $minEnd->getInclusiveEnd()) {
				return null;
			} else {
				return IntRange::fromTwoRanges($maxStart, $minEnd);
			}
		}

		/**
		 * @param IntRange $another another IntRange
		 * @return bool true if this instance and $another overlap
		 */
		public function overlaps(IntRange $another) : bool {
			return $this->intersect($another) !== null;
		}


		/**
		 * @param IntRange $another another IntRange
		 * @return bool true if this is left adjacent to $another (e.g. this=[8,10] and another=[1,7])
		 * @see IntRange::isAdjacent()
		 */
		public function isLeftAdjacentOf(IntRange $another) : bool {
			return $this->isEndFinite() && $another->isStartFinite() && $this->getInclusiveEnd() === $another->getExclusiveStart();
		}

		/**
		 * @param IntRange $another another IntRange
		 * @return bool true if this is right adjacent to $another (e.g. this=[1,7] and another=[8,10])
		 * @see IntRange::isAdjacent()
		 */
		public function isRightAdjacentOf(IntRange $another) : bool {
			return $another->isEndFinite() && $this->isStartFinite() && $another->getInclusiveEnd() === $this->getExclusiveStart();
		}

		/**
		 * Checks if this instance is adjacent to another. This property is commutative.
		 * The adjacency is when the start of one range is the the next number after the end of the other one
		 * @param IntRange $another another IntRange
		 * @return bool true if this instance and $another are adjacent to each other (e.g. this=[1,7] and another=[8,10] or viceversa)
		 */
		public function isAdjacent(IntRange $another) : bool {
			return $this->isLeftAdjacentOf($another) || $this->isRightAdjacentOf($another);
		}

		/**
		 * @param IntRange $another another IntRange
		 * @return bool true if the two ranges can be combined
		 * @see IntRange::combine()
		 */
		public function canCombine(IntRange $another) : bool {
			return $this->overlaps($another) || $this->isAdjacent($another);
		}

		/**
		 * Combines this IntRange and another one. This operation can only be done if the two ranges overlap or are adjacent
		 * @param IntRange $another another IntRange
		 * @return IntRange the combined IntRange
		 * @throws \DomainException if the two IntRange(s) cannot be combined together
		 * @see IntRange::canCombine()
		 */
		public function combine(IntRange $another) : IntRange {
			if (!$this->canCombine($another)) {
				throw new \DomainException('The two IntRanges must overlap or be adjacent! Use merge() instead');
			}
			if (IntRange::compareStarts($this, $another) <= 0) {
				$minStart = $this;
			} else {
				$minStart = $another;
			}
			if (IntRange::compareEnds($this, $another) >= 0) {
				$maxEnd = $this;
			} else {
				$maxEnd = $another;
			}
			if ($minStart === $maxEnd) {
				return $minStart;
			} else {
				return IntRange::fromTwoRanges($minStart, $maxEnd);
			}
		}

		/**
		 * Merges this instance and another together. Calls combine() if the two ranges can be combined.
		 * @param IntRange $another another instance
		 * @return IntSet the merged IntSet
		 * @see IntRange::combine()
		 */
		public function merge(IntRange $another) : IntSet {
			if ($this->overlaps($another) || $this->isAdjacent($another)) {
				return $this->combine($another);
			} else {
				return IntRanges::ranges($this, $another);
			}
		}

		/**
		 * @param IntRange $another another IntRange
		 * @return bool true if all the integers represented by this are contained in $another
		 */
		public function isSubRangeOf(IntRange $another) : bool {
			$intersection = $this->intersect($another);
			return $intersection !== null && $intersection->equals($this);
		}

		/**
		 * @param IntRange $another another IntRange
		 * @return bool true if all the integers represented by $another are contained in this
		 */
		public function isSuperRangeOf(IntRange $another) : bool {
			return $another->isSubRangeOf($this);
		}

		/**
		 * @return int the inclusive start value
		 * @throws \LogicException if the start value is not finite
		 */
		public function getInclusiveStart() : int {
			if ($this->start === null) {
				throw new \LogicException('The start value must be finite');
			}
			return $this->startInclusive ? $this->start : $this->start + 1;
		}

		/**
		 * @return int the exclusive start value
		 * @throws \LogicException if the start value is not finite
		 */
		public function getExclusiveStart() : int {
			if ($this->start === null) {
				throw new \LogicException('The start value must be finite');
			}
			return $this->startInclusive ? $this->start - 1 : $this->start;
		}

		/**
		 * @return int the inclusive end value
		 * @throws \LogicException if the end value is not finite
		 */
		public function getInclusiveEnd() : int {
			if ($this->end === null) {
				throw new \LogicException('The end value must be finite');
			}
			return $this->endInclusive ? $this->end : $this->end - 1;
		}

		/**
		 * @return int the exclusive end value
		 * @throws \LogicException if the end value is not finite
		 */
		public function getExclusiveEnd() : int {
			if ($this->end === null) {
				throw new \LogicException('The end value must be finite');
			}
			return $this->endInclusive ? $this->end + 1 : $this->end;
		}

		/**
		 * @return IntRange this instance if it has an inclusive start, or a new equivalent instance with an inclusive start
		 */
		public function withInclusiveStart() : IntRange {
			return $this->startInclusive ? $this : self::finiteWithInclusivityFix($this->start + 1, $this->end, true, $this->endInclusive);
		}

		/**
		 * @return IntRange this instance if it has an exclusive start, or a new equivalent instance with an exclusive start
		 */
		public function withExclusiveStart() : IntRange {
			return $this->startInclusive ? self::finiteWithInclusivityFix($this->start - 1, $this->end, false, $this->endInclusive) : $this;
		}

		/**
		 * @return IntRange this instance if it has an inclusive end, or a new equivalent instance with an inclusive end
		 */
		public function withInclusiveEnd() : IntRange {
			return $this->endInclusive ? $this : self::finiteWithInclusivityFix($this->start, $this->end - 1, $this->startInclusive, true);
		}

		/**
		 * @return IntRange this instance if it has an exclusive end, or a new equivalent instance with an exclusive end
		 */
		public function withExclusiveEnd() : IntRange {
			return $this->endInclusive ? self::finiteWithInclusivityFix($this->start, $this->end + 1, $this->startInclusive, false) : $this;
		}

		/**
		 * {@inheritDoc}
		 */
		public function isContained(int $int) : bool {
			return ($this->start === null || ($this->startInclusive ? $int >= $this->start : $int > $this->start)) && ($this->end === null || ($this->endInclusive ? $int <= $this->end : $int < $this->end));
		}

		/**
		 * {@inheritDoc}
		 */
		public function __toString() : string {
			$ret = '';
			if ($this->startInclusive) {
				$ret .= '[';
			} else {
				$ret .= '(';
			}
			if ($this->start === null) {
				$ret .= '-∞';
			} else {
				$ret .= $this->start;
			}
			$ret .= ', ';
			if ($this->end === null) {
				$ret .= '+∞';
			} else {
				$ret .= $this->end;
			}
			if ($this->endInclusive) {
				$ret .= ']';
			} else {
				$ret .= ')';
			}
			return $ret;
		}

		public function string() : string {
			if ($this->isAllIntegers()) {
				return '*';
			} elseif ($this->isSingle()) {
				return (string)$this->getInclusiveStart();
			}

			$ret = '';
			if ($this->isStartInfinite()) {
				$ret .= '*';
			} else {
				$ret .= $this->getInclusiveStart();
			}
			$ret .= '-';
			if ($this->isEndInfinite()) {
				$ret .= '*';
			} else {
				$ret .= $this->getInclusiveEnd();
			}
			return $ret;
		}

		/**
		 * @param IntRange $start the range to use the start of
		 * @param IntRange $end the range to use the end of
		 * @return IntRange a new instance using the first range's start and the second range's end
		 */
		private static function fromTwoRanges(IntRange $start, IntRange $end) : IntRange {
			return new IntRange($start->getStart(), $end->getEnd(), $start->isStartInclusive(), $end->isEndInclusive());
		}

		/**
		 * @param int $number the number
		 * @return IntRange a new IntRange instance representing a single number
		 */
		public static function single(int $number) : IntRange {
			return IntRange::finite($number, $number);
		}

		private static function finiteWithInclusivityFix(int $start, int $end, bool $startInclusive, bool $endInclusive) : IntRange {
			if ($start === $end) {
				return IntRange::single($start);
			} else {
				return IntRange::finite($start, $end, $startInclusive, $endInclusive);
			}
		}

		/**
		 * @param int $start the start value
		 * @param int $end the end value
		 * @param bool $startInclusive weather the start value is inclusive. Defaults to true.
		 * @param bool $endInclusive weather the end value is inclusive. Defaults to true.
		 * @return IntRange a new instance representing a finite range of values
		 */
		public static function finite(int $start, int $end, bool $startInclusive = true, bool $endInclusive = true) : IntRange {
			return new IntRange($start, $end, $startInclusive, $endInclusive);
		}

		/**
		 * @param int $end the end value
		 * @param bool $endInclusive the end value is inclusive. Defaults to true.
		 * @return IntRange a new instance with the start at -infinity and the given end value
		 */
		public static function startOpen(int $end, bool $endInclusive = true) : IntRange {
			return new IntRange(null, $end, false, $endInclusive);
		}

		/**
		 * @param int $start the start value
		 * @param bool $startInclusive the start value is inclusive. Defaults to true.
		 * @return IntRange a new instance with the end at +infinity and the given start value
		 */
		public static function endOpen(int $start, bool $startInclusive = true) : IntRange {
			return new IntRange($start, null, $startInclusive, false);
		}

		/**
		 * @return IntRange a new instance representing the range (-inf, +inf)
		 */
		public static function allIntegers() : IntRange {
			return new IntRange(null, null, false, false);
		}

		/**
		 * {@inheritDoc}
		 */
		public function jsonSerialize() : array {
			$ret = [];
			if ($this->isStartInclusive()) {
				$ret['startInclusive'] = $this->start;
			} else {
				$ret['startExclusive'] = $this->start;
			}
			if ($this->isEndInclusive()) {
				$ret['endInclusive'] = $this->end;
			} else {
				$ret['endExclusive'] = $this->end;
			}
			return $ret;
		}

		/**
		 * {@inheritDoc}
		 * @return IntRange the parsed IntRange
		 */
		public static function parse(mixed $value) : IntSet {
			if (!\is_array($value)) {
				throw new IntSetParseException('value must be an array, ' . ObjectsUtils::getType('found'));
			}

			$startInclusive = true;
			$endInclusive = true;
			if (array_key_exists('start', $value)) {
				$kStart = 'start';
			} elseif (array_key_exists('startInclusive', $value)) {
				$kStart = 'startInclusive';
			} elseif (array_key_exists('startExclusive', $value)) {
				$kStart = 'startExclusive';
				$startInclusive = false;
			} else {
				throw new IntSetParseException("The given array doesn't contain the start value");
			}
			if (array_key_exists('end', $value)) {
				$kEnd = 'end';
			} elseif (array_key_exists('endInclusive', $value)) {
				$kEnd = 'endInclusive';
			} elseif (array_key_exists('endExclusive', $value)) {
				$kEnd = 'endExclusive';
				$endInclusive = false;
			} else {
				throw new IntSetParseException("The given array doesn't contain the end value");
			}

			$start = $value[$kStart];
			if ($start !== null && !\is_int($start)) {
				throw new IntSetParseException('start is not an int');
			}

			$end = $value[$kEnd];
			if ($end !== null && !\is_int($end)) {
				throw new IntSetParseException('end is not an int');
			}

			if ($start !== null && $end !== null && $end < $start) {
				throw new IntSetParseException('end < start');
			}

			return new IntRange($start, $end, $startInclusive, $endInclusive);
		}

		/**
		 * @throws IntSetParseException
		 */
		public static function parseString(string $string) : IntRange {
			if ($string === '*') {
				return IntRange::allIntegers();
			} else {
				$single = StringUtils::parseIntOrNull($string);
				if ($single !== null) {
					return IntRange::single($single);
				}
			}

			if (preg_match('/(\*|[+-]?\d+)-(\*|[+-]?\d+)/', $string, $matches) !== 1 || \count($matches) !== 3) {
				throw new IntSetParseException('Invalid syntax');
			}

			$a = $matches[1];
			/** @noinspection MultiAssignmentUsageInspection */
			$b = $matches[2];

			if ($a === '*') {
				$start = null;
			} else {
				$start = StringUtils::parseIntOrNull($a);
				if ($start === null) {
					throw new IntSetParseException("Invalid start '$a'");
				}
			}
			if ($b === '*') {
				$end = null;
			} else {
				$end = StringUtils::parseIntOrNull($b);
				if ($end === null) {
					throw new IntSetParseException("Invalid end '$b'");
				}
			}

			if ($start !== null && $end !== null && $end < $start) {
				throw new IntSetParseException('end < start');
			}

			return new IntRange($start, $end, $start !== null, $end !== null);
		}


		protected function iterator() : IntSetIterator {
			$me = $this;
			return new class($me) extends IntSetIterator {

				private IntRange $range;
				private ?int $current = null;

				public function __construct(IntRange $range) {
					$this->range = $range;
				}

				public function current() : int {
					return $this->current;
				}

				public function doNext() : void {
					$this->current++;
				}

				public function valid() : bool {
					return $this->current <= $this->range->getInclusiveEnd();
				}

				public function rewind() : void {
					$this->current = $this->range->getInclusiveStart();
				}
			};
		}
	}