<?php
	declare(strict_types=1);


	namespace com\femastudios\utils\core\intSet;


	abstract class IntSetIterator implements \Iterator {

		private int $index = 0;

		public abstract function current() : int;

		public function next() : void {
			$this->doNext();
			$this->index++;
		}

		public abstract function doNext() : void;

		public function key() : int {
			return $this->index;
		}

		public abstract function valid() : bool;

		public abstract function rewind() : void;
	}