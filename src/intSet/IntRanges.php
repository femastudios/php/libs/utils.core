<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core\intSet;

	use com\femastudios\utils\core\ObjectsUtils;

	final class IntRanges extends IntSet {

		/** @var IntRange[] */
		private array $ranges;
		private ?IntRanges $simplified = null;

		private function __construct(IntRange ...$ranges) {
			if (\count($ranges) === 0) {
				throw new \DomainException('Cannot create empty IntRanges. Use EmptyIntSet instead.');
			}
			$this->ranges = array_values($ranges);
		}

		/**
		 * @return IntRange[] the ranges representing this instance
		 */
		public function getRanges() : array {
			return $this->ranges;
		}

		/**
		 * @return bool true if the set of ranges is already simplified
		 */
		public function isSimplified() : bool {
			return $this->simplified() === $this;
		}

		/**
		 * Creates a simplified version of this instance, merging the IntRange(s) that overlap.
		 * @return IntRanges the simplified instance. Can also return itself.
		 */
		public function simplified() : IntRanges {
			if ($this->simplified === null) {
				$arr = $this->ranges;
				usort($arr, static function ($a, $b) {
					return IntRange::compareStarts($a, $b);
				});
				for ($i = 0; $i < \count($arr) - 1; $i++) {
					if ($arr[$i]->overlaps($arr[$i + 1])) {
						$arr[$i] = $arr[$i]->combine($arr[$i + 1]);
						array_splice($arr, $i + 1, 1);
						$i--;
					}
				}
				$this->simplified = IntRanges::sameRanges($arr, $this->ranges) ? $this : new IntRanges(...$arr); //sameRanges() optimization needed for the method isSimplified()
				$this->simplified->simplified = $this->simplified; //Self caching
			}
			return $this->simplified;
		}

		/**
		 * @param IntRange[] $ranges1
		 * @param IntRange[] $ranges2
		 * @return bool
		 */
		private static function sameRanges(array $ranges1, array $ranges2) : bool {
			$cnt = \count($ranges1);
			if ($cnt !== \count($ranges2)) {
				return false;
			} else {
				for ($i = 0; $i < $cnt; $i++) {
					if (!$ranges1[$i]->equals($ranges2[$i])) {
						return false;
					}
				}
				return true;
			}
		}

		/**
		 * {@inheritDoc}
		 */
		public function equals(IntSet $another) : bool {
			if ($another instanceof self) {
				return IntRanges::sameRanges($this->simplified()->ranges, $another->simplified()->ranges);
			} elseif ($another instanceof IntRange) {
				return $another->equals($this);
			} else {
				throw new \InvalidArgumentException('Unsupported IntSet of type ' . \get_class($another));
			}
		}

		/**
		 * {@inheritDoc}
		 */
		public function __toString() : string {
			return implode(' ∪ ', $this->ranges);
		}

		/**
		 * {@inheritDoc}
		 */
		public function isContained(int $int) : bool {
			foreach ($this->ranges as $range) {
				if ($range->isContained($int)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * {@inheritDoc}
		 */
		public function getCardinality() : ?int {
			$sum = 0;
			foreach ($this->simplified()->ranges as $range) {
				$card = $range->getCardinality();
				if ($card === null) {
					return null;
				} else {
					$sum += $card;
				}
			}
			return $sum;
		}

		/**
		 * {@inheritDoc}
		 */
		public function jsonSerialize() : array {
			$ret = [];
			foreach ($this->ranges as $range) {
				$ret[] = $range->jsonSerialize();
			}
			return $ret;
		}

		public function string() : string {
			return implode(',', array_map(static function (IntRange $ir) {
				return $ir->string();
			}, $this->ranges));
		}

		/**
		 * @throws IntSetParseException
		 */
		public static function parseString(string $string) : IntRanges {
			$exploded = explode(',', $string);
			if ($exploded === false) {
				throw new IntSetParseException('Empty string');
			}
			$ranges = [];
			foreach ($exploded as $part) {
				$ranges[] = IntRange::parseString($part);
			}
			return new IntRanges(...$ranges);
		}

		/**
		 * {@inheritdoc}
		 * @return IntRanges the parsed IntRanges
		 */
		public static function parse(mixed $value) : IntSet {
			if (!\is_array($value)) {
				throw new IntSetParseException('value must be an array, ' . ObjectsUtils::getType('found'));
			}
			$ranges = [];
			foreach ($value as $v) {
				$ranges[] = IntRange::parse($v);
			}
			return self::ranges(...$ranges);
		}

		/**
		 * Constructs a new instance given a list of IntRange(s)
		 * @param IntRange ...$ranges
		 * @return IntRanges the new instance
		 */
		public static function ranges(IntRange ...$ranges) : IntRanges {
			return new IntRanges(...$ranges);
		}

		public function isInfinite() : bool {
			foreach ($this->ranges as $range) {
				if ($range->isInfinite()) {
					return true;
				}
			}
			return false;
		}

		protected function iterator() : IntSetIterator {
			$simplified = $this->simplified();
			return new class($simplified) extends IntSetIterator {

				private $rng, $ranges;
				/** @var int */
				private $current, $currentRangeMax;

				public function __construct(IntRanges $rng) {
					$this->rng = $rng;
				}

				public function current() : int {
					return $this->current;
				}

				public function doNext() : void {
					if ($this->current < $this->currentRangeMax) {
						$this->current++;
					} else {
						$this->nextRange();
					}
				}

				public function valid() : bool {
					return $this->currentRangeMax !== null;
				}

				private function nextRange() : void {
					if (\count($this->ranges) === 0) {
						$this->currentRangeMax = null;
					} else {
						/** @var IntRange $nextRange */
						$nextRange = array_splice($this->ranges, 0, 1)[0];
						$this->current = max($this->current, $nextRange->getInclusiveStart());
						$this->currentRangeMax = $nextRange->getInclusiveEnd();
					}
				}

				public function rewind() : void {
					$this->ranges = $this->rng->getRanges();
					$this->current = null; //max(null, int) -> int
					$this->nextRange();
				}

			};
		}

	}