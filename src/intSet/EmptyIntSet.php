<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core\intSet;

	use com\femastudios\utils\core\ObjectsUtils;

	final class EmptyIntSet extends IntSet {

		/**
		 * {@inheritDoc}
		 */
		public function isContained(int $int) : bool {
			return false;
		}

		/**
		 * {@inheritDoc}
		 */
		public function getCardinality() : ?int {
			return 0;
		}

		/**
		 * {@inheritDoc}
		 */
		public function equals(IntSet $another) : bool {
			return $another instanceof self;
		}

		/**
		 * {@inheritDoc}
		 */
		public function __toString() : string {
			return '∅';
		}

		/**
		 * {@inheritDoc}
		 * @return object
		 */
		public function jsonSerialize() : object {
			return (object)[];
		}

		/**
		 * {@inheritDoc}
		 * @return EmptyIntSet the parsed EmptyIntSet
		 */
		public static function parse(mixed $value) : IntSet {
			if ($value === []) {
				return new self();
			} elseif (\is_array($value)) {
				throw new IntSetParseException('value must be an empty array, found ' . \count($value) . ' items');
			} else {
				throw new IntSetParseException('value must be an empty array, found ' . ObjectsUtils::getType($value));
			}
		}

		public function isInfinite() : bool {
			return false;
		}

		protected function iterator() : IntSetIterator {
			return new class() extends IntSetIterator {

				public function current() : int {
					throw new \LogicException();
				}

				public function doNext() : void {
					throw new \LogicException();
				}

				public function valid() : bool {
					return false;
				}

				public function rewind() : void {
				}
			};
		}


	}