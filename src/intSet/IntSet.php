<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core\intSet;

	/**
	 * An interface that a class that represents a set of integers (both finite and infinite) must implement
	 *
	 * @package com\femastudios\utils\core\intSet
	 */
	abstract class IntSet implements \JsonSerializable, \IteratorAggregate {

		/**
		 * @param int $int an integer
		 * @return bool true if the given integer is a member of this IntSet
		 */
		public abstract function isContained(int $int) : bool;

		/**
		 * @return int|null the number of items represented by this set. null if it's infinity
		 */
		public abstract function getCardinality() : ?int;

		/**
		 * @param IntSet $another another IntSet
		 * @return bool true if they represent the same set of integers
		 */
		public abstract function equals(IntSet $another) : bool;

		/**
		 * @return string a string representation of this IntSet
		 */
		public abstract function __toString() : string;

		public function getIterator() : IntSetIterator {
			if ($this->isInfinite()) {
				throw new \LogicException('Can only iterate finite ranges!');
			}
			return $this->iterator();
		}

		protected abstract function iterator() : IntSetIterator;

		/**
		 * @return bool true if this set has a finite number of items
		 */
		public function isFinite() : bool {
			return !$this->isInfinite();
		}

		/**
		 * @return bool true if this set has an infinite number of items
		 */
		public abstract function isInfinite() : bool;

		/**
		 * Parses a value obtained through the jsonSerialize() method
		 * @param $value mixed the value to parse
		 * @return IntSet a new IntSet instance
		 * @throws IntSetParseException if the given value is not valid
		 */
		public abstract static function parse(mixed $value) : IntSet;

	}