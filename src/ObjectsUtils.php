<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	final class ObjectsUtils {

		private function __construct() {
			throw new \LogicException();
		}

		/**
		 * Tests the given value against nullity
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param mixed $value the value to test
		 * @throws \DomainException if the value is <code>null</code>
		 * @return mixed the passed value
		 */
		public static function requireNotNull(string $name, mixed $value) : mixed {
			if ($value === null) {
				throw new \DomainException("$name === null");
			}
			return $value;
		}

		/**
		 * Checks a string for nullity and emptiness
		 * @param null|string $value the value to test
		 * @param bool $trim weather to trim the string before testing for emptiness. Defaults to true
		 * @return bool true if it's null or empty
		 */
		public static function isNullOrEmpty(?string $value, bool $trim = true) : bool {
			return $value === null || ($trim ? trim($value) : $value) === '';
		}

		/**
		 * Tests the given value against a specified length range
		 * @param string $value the value to test
		 * @param int $minLen the min length. Defaults to 0
		 * @param int|null $maxLen the max length. null to do not limit max length. Defaults to null.
		 * @return string the passed string
		 * @throws \LengthException if the given string is not in the specified length range
		 */
		public static function requireStringLength(string $value, int $minLen = 0, int $maxLen = null) : string {
			$l = mb_strlen($value);
			if ($maxLen !== null && $l > $maxLen) {
				throw new \LengthException("The value exceeds the maximum length of $maxLen with its $l characters!");
			} elseif ($minLen !== null && $l < $minLen) {
				throw new \LengthException("The value exceeds the minimum length of $minLen with its $l characters!");
			}
			return $value;
		}

		/**
		 * Forces a string to be either non-empty or null
		 * @param null|string $value the value to force
		 * @param bool $trim weather to trim the string before testing for emptiness. Defaults to true
		 * @return null|string null if the given string is null or empty, $value otherwise
		 */
		public static function forceNonEmptyString(?string $value, bool $trim = true) : ?string {
			if (self::isNullOrEmpty($value, $trim)) {
				return null;
			} else {
				return $value;
			}
		}

		/**
		 * @param $value mixed the value to test
		 * @return bool true if the given value can be used in a for-each loop
		 */
		public static function isIterable($value) : bool {
			return \is_array($value) || $value instanceof \Traversable;
		}

		/**
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param array $value the value to test
		 * @param string $typeKey the type of the keys
		 * @param string $typeValue the type of the values
		 * @return array the given value
		 */
		public static function requireMapOf(string $name, array $value, string $typeKey, string $typeValue) : array {
			foreach ($value as $key => $val) {
				self::requireType("All keys of $name", $key, $typeKey);
				self::requireType("All values of $name", $val, $typeValue);
			}
			return $value;
		}

		/**
		 * @param string $type the type name
		 * @return bool true if the given type is a primitive PHP type (e.g. string, int, etc.)
		 */
		public static function isBaseType(string $type) : bool {
			return
				$type === 'string' ||
				$type === 'int' ||
				$type === 'float' || $type === 'double' ||
				$type === 'array' ||
				$type === 'bool' ||
				$type === 'resource' ||
				$type === 'object' ||
				$type === 'callable' ||
				$type === 'scalar';
		}

		/**
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param array $value the value to test
		 * @param string $type the type
		 * @param bool $allowNullValues weather to allow null. Defaults to false.
		 * @return array the given value
		 */
		public static function requireArrayOf(string $name, array $value, string $type, bool $allowNullValues = false) : array {
			foreach ($value as $k => $val) {
				ObjectsUtils::requireType($name . '[' . $k . ']', $val, $type, $allowNullValues);
			}
			return $value;
		}

		/**
		 * Returns the type name of the given value. Uses a combination of gettype() and get_class(). Useful for debug/error messages.<br>
		 * Examples:
		 * <ul>
		 *    <li>getType("a") -> "string"</li>
		 *    <li>getType(5) -> "int"</li>
		 *    <li>getType(new \com\example\package\Class()) -> "com\example\package\Class"</li>
		 *    <li>getType(new \Exception()) -> "Exception"</li>
		 * </ul>
		 * @param $value mixed the value to obtain the type of
		 * @return string the type name
		 */
		public static function getType($value) : string {
			if (\is_object($value)) {
				return \get_class($value);
			} else {
				return \gettype($value);
			}
		}

		/**
		 * Requires a value to be of the specified types
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param $value mixed the value to test
		 * @param string[]|string[][] $types the types to require. Each array found will be treated as a parenthesis with $or inverted. (e.g. [string, [A, B]], or=true will be trated as string || (A && B))
		 * @param bool $or weather to do an OR or an AND. Defaults to true (OR)
		 * @param bool $allowNull weather to allow null. Defaults to false.
		 * @return mixed the given value
		 * @throws \DomainException if the value is null and null is not allowed
		 * @throws \InvalidArgumentException if the value is not of the given type
		 */
		public static function requireTypes(string $name, $value, array $types, bool $or = true, bool $allowNull = false) {
			if (\count($types) > 0) {
				foreach ($types as $type) {
					if (\is_array($type)) {
						self::requireTypes($name, $value, $type, !$or, $allowNull);
					} elseif ($or) {
						try {
							ObjectsUtils::requireType($name, $value, $type, $allowNull);
							return $value;
						} catch (\InvalidArgumentException) {
						}
					} else {
						ObjectsUtils::requireType($name, $value, $type, $allowNull);
					}
				}
				if ($or) {
					throw new \InvalidArgumentException("$name must be one of the following types: " . implode(', ', $types) . '. Found ' . self::getType($value));
				}
			}
			return $value;
		}

		/**
		 * Requires a value to be of the specified type
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param mixed $value the value to test
		 * @param string $type the type to require
		 * @param bool $allowNull weather to allow null. Defaults to false.
		 * @return mixed the given value
		 * @throws \DomainException if the value is null and null is not allowed
		 * @throws \InvalidArgumentException if the value is not of the given type
		 */
		public static function requireType(string $name, mixed $value, string $type, bool $allowNull = false) : mixed {
			if (!$allowNull) {
				ObjectsUtils::requireNotNull($name, $value);
			}
			if ($value !== null) {
				switch ($type) {
					case 'string':
						self::requireString($name, $value);
						break;
					case 'bool':
						self::requireBool($name, $value);
						break;
					case 'int':
						self::requireInt($name, $value);
						break;
					case 'double':
					case 'float':
						self::requireFloat($name, $value);
						break;
					case 'array':
						self::requireArray($name, $value);
						break;
					case 'resource':
						self::requireResource($name, $value);
						break;
					case 'object':
						self::requireObject($name, $value);
						break;
					case 'callable':
						self::requireCallable($name, $value);
						break;
					case 'scalar':
						self::requireScalar($name, $value);
						break;
					default:
						self::requireInstance($name, $value, $type);
						break;
				}
			}
			return $value;
		}

		/**
		 * Tests if the given type exists
		 * @param string $type the type
		 * @return bool true if the type exists
		 */
		public static function typeExists(string $type) : bool {
			if (ObjectsUtils::isBaseType($type)) {
				return true;
			} else {
				return class_exists($type) || interface_exists($type);
			}
		}

		/**
		 * Forces a given value to be converted to a bool
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param $value bool|int|string the value to force. Accepts bool, int and "true", "false" strings
		 * @return bool the forced value
		 * @throws \InvalidArgumentException if the given value cannot be converted to string
		 */
		public static function forceBool(string $name, bool|int|string $value) : bool {
			if (\is_bool($value)) {
				return $value;
			} elseif (\is_int($value)) {
				return $value !== 0;
			} elseif ($value === 'true' || $value === '1') {
				return true;
			} elseif ($value === 'false' || $value === '0') {
				return false;
			} else {
				throw new \InvalidArgumentException("$name is an invalid boolean");
			}
		}


        /**
         * Like {@method ObjectsUtils::forceInt}, but returns $defaultValue on failure
         * @param $value int|string the value to force
         * @param int|null $defaultValue the value to return if the given value is invalid. Defaults to null
         * @return int|null the forced value or $defaultValue
         */
		public static function optForceInt($value, ?int $defaultValue = null) : ?int {
			try {
				return ObjectsUtils::forceInt('', $value);
			} catch (\DomainException|\InvalidArgumentException) {
				return $defaultValue;
			}
		}

		/**
		 * Forces a given value to be converted to an int
		 * @param string $name the name of the tested value. Used for the exception message
		 * @param $value int|string the value to force
		 * @return int the forced value
		 * @throws \DomainException if the given value is an invalid int string
		 */
		public static function forceInt(string $name, $value) : int {
			ObjectsUtils::requireNotNull($name, $value);
			if (\is_int($value)) {
				return $value;
			} elseif (!\is_string($value)) {
				throw new \InvalidArgumentException('The value, if not an int, must be a string representing an integer!');
			} elseif (preg_match("/^\s*[+-]?\d+\s*$/", $value) === 1) {
				return (int)$value;
			} else {
				throw new \DomainException("$name is an invalid int");
			}
		}

		/**
		 * Returns the non-null element. If both are not null, the preferFirst parameter takes over
		 * @param mixed $a the first element
		 * @param mixed $b the second element
		 * @param bool $preferFirst weather to prefer the first one if both are non-null
		 * @return mixed the non-null element. If both are null, null is returned. If both are non-null, the first is returned if preferFirst is true, otherwise the second is returned
		 */
		public static function getNonNull($a, $b, bool $preferFirst) : mixed {
			if ($a === null) {
				return $b;
			} elseif ($b === null || $preferFirst) {
				return $a;
			} else {
				return $b;
			}
		}

		/**
		 * Checks if the given value can be casted safely to string, like this:<br>
		 * <code>(string) $value</code>
		 * @param mixed $value a value to test
		 * @return bool true if value can be casted to string
		 */
		public static function canBeCastedToString($value) : bool {
			return (!\is_array($value)) && (
					(!\is_object($value) && settype($value, 'string') !== false) ||
					(\is_object($value) && method_exists($value, '__toString'))
				);
		}

		/**
		 * @param $value mixed a value
		 * @param bool $encloseInQuotesIfString weather to enclose the result in quotes if not a primitive value (int, float, etc.) or a type. Defaults to false
		 * @return string its string representation. If its string representation cannot be found, its type from {@link ObjectsUtils::getType} will be returned
		 */
		public static function toString($value, bool $encloseInQuotesIfString = false) : string {
			if ($value === null) {
				return 'null';
			} elseif ($value instanceof \DateTime) {
				return self::toString($value->format(DATE_ATOM));
			} elseif (\is_array($value)) {
				$ret = [];
				foreach ($value as $k => $v) {
					$ret[] = self::toString($k, true) . ' => ' . self::toString($v, true);
				}
				return '[' . implode(', ', $ret) . ']';
			} elseif (self::canBeCastedToString($value)) {
				if (\is_bool($value)) {
					return $value ? 'true' : 'false';
				} elseif (\is_int($value) || \is_float($value) || \is_resource($value) || !$encloseInQuotesIfString) {
					return (string)$value;
				} else {
					return '\'' . $value . '\'';
				}
			} else {
				return ObjectsUtils::getType($value);
			}
		}

		public static function requireString(string $name, $value) : void {
			if (!\is_string($value)) {
				throw new \InvalidArgumentException("$name must be a string, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireBool(string $name, $value) : void {
			if (!\is_bool($value)) {
				throw new \InvalidArgumentException("$name must be a bool, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireInt(string $name, $value) : void {
			if (!\is_int($value)) {
				throw new \InvalidArgumentException("$name must be an int, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireFloat(string $name, $value) : void {
			if (!\is_float($value)) {
				throw new \InvalidArgumentException("$name must be a float, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireArray(string $name, $value) : void {
			if (!\is_array($value)) {
				throw new \InvalidArgumentException("$name must be an array, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireResource(string $name, $value) : void {
			if (!\is_resource($value)) {
				throw new \InvalidArgumentException("$name must be a resource, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireObject(string $name, $value) : void {
			if (!\is_object($value)) {
				throw new \InvalidArgumentException("$name must be an object, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireCallable(string $name, $value) : void {
			if (!\is_callable($value)) {
				throw new \InvalidArgumentException("$name must be a callable, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireScalar(string $name, $value) : void {
			if (!is_scalar($value)) {
				throw new \InvalidArgumentException("$name must be a scalar, found " . ObjectsUtils::getType($value));
			}
		}

		public static function requireInstance(string $name, $value, string $type) : void {
			if (!is_a($value, $type)) {
				throw new \InvalidArgumentException("$name must be an object of class $type, found " . ObjectsUtils::getType($value) . '!');
			}
		}
	}
