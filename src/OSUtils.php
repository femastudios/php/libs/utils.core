<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	/**
	 * Class OSUtils
	 * @package com\femastudios\utils\core
	 */
	final class OSUtils {

		private function __construct() {
			throw new \LogicException();
		}

		/**
		 * @return bool true if PHP is running on a Windows environment
		 */
		public static function isWindows() : bool {
			return strncasecmp(PHP_OS, 'WIN', 3) === 0;
		}

		/**
		 * @return bool true if PHP is running on a Unix-like environment
		 */
		public static function isUnix() : bool {
			return DIRECTORY_SEPARATOR === '/';
		}
	}
