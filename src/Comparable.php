<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	/**
	 * Class that defines an object that can be compared
	 * @package fema\utils
	 */
	interface Comparable {

		/**
		 * Compares this object to another one
		 * @param $another mixed the other object
		 * @return int 0 if the two objects are equal, -1 if this object is less than the given one, +1 it is greater
		 * @throws \InvalidArgumentException if the given object is not correct
		 */
		public function compareTo(mixed $another) : int;

	}