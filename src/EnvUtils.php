<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	final class EnvUtils {

		private function __construct() {
			throw new \LogicException();
		}

		public static function optEnv(string $envName, bool $localOnly = false, ?string $defaultValue = null) : ?string {
			$ret = getenv($envName, $localOnly);
			if ($ret === false) {
				return $defaultValue;
			} else {
				return $ret;
			}
		}

		public static function getEnv(string $envName, bool $localOnly = false) : string {
			$ret = EnvUtils::optEnv($envName, $localOnly);
			if ($ret === null) {
				throw new \RuntimeException("Environment variable '$envName' not found");
			} else {
				return $ret;
			}
		}

		public static function hasEnv(string $envName, bool $localOnly = false) : bool {
			return EnvUtils::optEnv($envName, $localOnly) !== null;
		}
	}