<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	final class RegexUtils {

		private function __construct() {
			throw new \LogicException();
		}

		private static function getBareIntRangeRegex(int $from, int $to) : string {
			$ranges = [$from];
			$increment = 1;
			$next = $from;
			$higher = true;

			while (true) {
				$next += $increment;
				if ($next + $increment > $to) {
					if ($next <= $to) {
						$ranges[] = $next;
					}
					$increment /= 10;
					$higher = false;
				} elseif ($next % ($increment * 10) === 0) {
					$ranges[] = $next;
					$increment = $higher ? $increment * 10 : $increment / 10;
				}
				if (!$higher && $increment < 10) {
					break;
				}
			}

			$ranges[] = $to + 1;

			$regex = '(?:0*(?:';

			for ($i = 0; $i < \count($ranges) - 1; $i++) {
				$strFrom = (string)$ranges[$i];
				$strTo = (string)($ranges[$i + 1] - 1);

				for ($j = 0, $len = \strlen($strFrom); $j < $len; $j++) {
					if ($strFrom[$j] === $strTo[$j]) {
						$regex .= $strFrom[$j];
					} else {
						$regex .= '[' . $strFrom[$j] . '-' . $strTo[$j] . ']';
					}
				}
				$regex .= '|';
			}

			//substr is good because we only put ASCII characters in $regex, and we now it (clap you hands!)
			return substr($regex, 0, -1) . '))';
		}


        /**
         * Constructs a regex that matches the given range of ints.<br>
         * This methods returns a bare regex, so you'll need to encapsulate it in a "/.../" (or "/^...$/" to match all the string)<br>
         * <br>
         * <b>BEWARE</b>: the size of the regex increments with the range, so it's a good idea not to use this method when ranges start getting big. Use instead a capturing group of digits (\d+) and check the range at runtime
         * @param int $from the lower bound (inclusive)
         * @param int $to the upper bound (inclusive)
         * @param bool $minusZeroValid weather the string "-0" is considered valid and matched by the regex. This boolean is disregarded if 0 is not in the given range. Defaults to true
         * @return string|null the regex
         */
		public static function getIntRangeRegex(int $from, int $to, bool $minusZeroValid = true) : ?string {
			if ($from > $to) {
				throw new \DomainException("Invalid range $from..$to: from > to");
			}
			$zeroRegex = ($minusZeroValid ? "[-\\+]?" : "\\+?") . '0+';

			//Checks if zero is in range and handles it in special case
			$fromIsZero = $from === 0;
			$toIsZero = $to === 0;
			if ($fromIsZero && $toIsZero) {
				return $zeroRegex;
			} elseif ($fromIsZero) {
				return "(?:$zeroRegex|" . RegexUtils::getIntRangeRegex(1, $to) . ')';
			} elseif ($toIsZero) {
				return "(?:$zeroRegex|" . RegexUtils::getIntRangeRegex($from, -1) . ')';
			} elseif ($from < 0 && $to > 0) {
				return '(?:' . RegexUtils::getIntRangeRegex($from, -1) . "|$zeroRegex|" . RegexUtils::getIntRangeRegex(1, $to) . ')';
			}

			//Here I'm sure that $from and $to are NOT zero
			if ($from > 0 && $to > 0) {
				return "\\+?" . RegexUtils::getBareIntRangeRegex($from, $to);
			} elseif ($from < 0 && $to < 0) {
				return '-' . RegexUtils::getBareIntRangeRegex(-$to, -$from);
			} else {
				throw new \LogicException('Illegal state');
			}
		}
	}
