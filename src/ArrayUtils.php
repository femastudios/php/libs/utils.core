<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	/**
	 * Class that defines various useful function to deal with PHP arrays
	 * @package com\femastudios\utils\core
	 */
	final class ArrayUtils {

		private function __construct() {
			throw new \LogicException();
		}

		/**
		 * Removes duplicates with the given array.<br>
		 * Order is preserved in a first-occurrence-fist-served manner<br>
		 * Keys are NOT preserved.
		 * @param array $array the array
		 * @param bool $strict weather to use === comparison, instead of ==. Defaults to true
		 * @return array the array without duplicates
		 */
		public static function removeDuplicates(array $array, bool $strict = true) : array {
			$ret = [];
			foreach ($array as $item) {
				if (!\in_array($item, $ret, $strict)) {
					$ret[] = $item;
				}
			}
			return $ret;
		}

		/**
		 * Checks if the given array contains all the elements of the second one
		 * @param array $a the first array
		 * @param array $b the second array
		 * @param bool $strict weather to use === comparison, instead of ==. Defaults to true
		 * @return bool true if all elements of b are contained in a
		 */
		public static function containsAll(array $a, array $b, bool $strict = true) : bool {
			foreach ($b as $item) {
				if (!\in_array($item, $a, $strict)) {
					return false;
				}
			}
			return true;
		}

		/**
		 * Checks weather an array is identical (aside from ordering) to another.<br>
		 * Examples:
		 * <ul>
		 *    <li>sameValues([1,2], [2,1]) -> TRUE</li>
		 *    <li>sameValues([1,2,3], [2,1]) -> FALSE</li>
		 *    <li>sameValues([1,2], [2,2,1]) -> FALSE</li>
		 * </ul>
		 * @param array $a the first array
		 * @param array $b the second array
		 * @param bool $strict true if the comparison between elements must be strict, false otherwise
		 * @return bool true if every element of a is present in b in the same quantity, and vice versa
		 */
		public static function sameValues(array $a, array $b, bool $strict = true) : bool {
			if (\count($a) !== \count($b)) {
				return false;
			} else {
				foreach ($a as $aVal) {
					$k = array_search($aVal, $b, $strict);
					if ($k === false) {
						return false;
					} else {
						unset($b[$k]);
					}
				}
				return true;
			}
		}

		/**
		 * Checks weather an array has the same set of values to another. Ordering and duplicates doesn't count.<br>
		 * Examples:
		 * <ul>
		 *    <li>sameValues([1,2], [2,1]) -> TRUE</li>
		 *    <li>sameValues([1,2,3], [2,1]) -> FALSE</li>
		 *    <li>sameValues([1,2], [2,2,1]) -> TRUE</li>
		 * </ul>
		 * @param array $a the first array
		 * @param array $b the second array
		 * @param bool $strict true if the comparison between elements must be strict, false otherwise
		 * @return bool true if every element of a is present in b, and vice versa
		 */
		public static function sameSet(array $a, array $b, bool $strict = true) : bool {
			foreach ($a as $av) {
				if (!\in_array($av, $b, $strict)) {
					return false;
				}
			}
			foreach ($b as $bv) {
				if (!\in_array($bv, $a, $strict)) {
					return false;
				}
			}
			return true;
		}

		/**
		 * @param array $array the array to obtain the head of
		 * @return mixed the head of the array (i.e. its first element)
		 */
		public static function head(array $array) : mixed {
			if (\count($array) === 0) {
				throw new \DomainException("empty array doesn't have head");
			} else {
				return array_values($array)[0];
			}
		}

		/**
		 * @param array $array the array to obtain the tail of
		 * @return array the tail of the array (i.e. all its elements in the same order except the first (head))
		 */
		public static function tail(array $array) : array {
			if (\count($array) === 0) {
				return [];
			} else {
				return \array_slice($array, 1);
			}
		}

		/**
		 * Obtains the item in the haystack array specified by the needle path
		 * @param int[]|string[] $needle the path of the item
		 * @param array $haystack the array to get the item from
		 * @return mixed the value
		 * @throws \OutOfBoundsException if a key is not found in an array
		 */
		public static function deepGet(array $needle, array $haystack) : mixed {
			$currentNeedle = ArrayUtils::head($needle);
			if (!array_key_exists($currentNeedle, $haystack)) {
				throw new \OutOfBoundsException('Field not found!');
			} else {
				$val = $haystack[$currentNeedle];
				$afterNeedle = ArrayUtils::tail($needle);
				if ($afterNeedle === []) {
					return $val;
				} elseif (!\is_array($val)) {
					throw new \OutOfBoundsException('Field not found!');
				} else {
					return ArrayUtils::deepGet($afterNeedle, $val);
				}
			}
		}

		/**
		 * Checks if an item is contained in the haystack array for the path specified by the needle array
		 * @param array $needle the path of the item
		 * @param array $haystack the array to get the item from
		 * @return bool if the item is contained
		 */
		public static function deepContains(array $needle, array $haystack) : bool {
			$currentNeedle = ArrayUtils::head($needle);
			if (!array_key_exists($currentNeedle, $haystack)) {
				return false;
			} else {
				$val = $haystack[$currentNeedle];
				$afterNeedle = ArrayUtils::tail($needle);
				if ($afterNeedle === []) {
					return true;
				} else {
					return \is_array($val) && ArrayUtils::deepContains($afterNeedle, $val);
				}
			}
		}

		/**
		 * Merges two arrays and removes duplicates.<br>
		 * Keys are NOT preserved<br>
		 * Order is preserved in a first-occurrence-fist-served manner
		 * @param array $a1 the first array
		 * @param array $a2 the second array
		 * @param bool $strict true if the comparison between elements must be strict, false otherwise
		 * @return array the merged array
		 */
		public static function mergeWithoutDuplicates(array $a1, array $a2, bool $strict = true) : array {
			$ret = [];
			foreach ($a1 as $i) {
				if (!\in_array($i, $ret, $strict)) {
					$ret[] = $i;
				}
			}
			foreach ($a2 as $i) {
				if (!\in_array($i, $ret, $strict)) {
					$ret[] = $i;
				}
			}
			return $ret;
		}

		/**
		 * Adds all the values of a2 in a1.
		 * Note: keys are NOT preserved
		 * @param array $a1 the array to push the items to
		 * @param array $a2 the array containing the items to push
		 */
		public static function pushAll(array &$a1, array $a2) : void {
			foreach ($a2 as $v) {
				$a1[] = $v;
			}
		}

		/**
		 * @param array $a The first array
		 * @param array $b The second array
		 * @param bool $strict true if the comparison between elements must be strict, false otherwise
		 * @param bool $preserveKeys weather to preserve keys or not
		 * @return array a new array containing all the values of $a not contained in $b
		 */
		public static function diff(array $a, array $b, bool $strict = true, bool $preserveKeys = false) : array {
			$ret = [];
			foreach ($a as $k => $v) {
				if (!\in_array($v, $b, $strict)) {
					if ($preserveKeys) {
						$ret[$k] = $v;
					} else {
						$ret[] = $v;
					}
				}
			}
			return $ret;
		}

		/**
		 * Makes a copy of the array. Any reference in the values/keys will be lost.
		 * @param array $arr the array to copy
		 * @return array the copy
		 */
		public static function copy(array $arr) : array {
			$ret = [];
			foreach ($arr as $k => $v) {
				$ret[$k] = $v;
			}
			return $ret;
		}

		/**
		 * Checks if two arrays have the same key/value pairs
		 * @param array $a the first array
		 * @param array $b the second array
		 * @param bool $strict true to use the === operator when comparing values, false to use ==
		 * @return bool true if the two arrays are the same, false otherwise
		 */
		public static function equals(array $a, array $b, bool $strict = true) : bool {
			if (\count($a) !== \count($b)) {
				return false;
			}
			foreach ($a as $k => $v) {
				/** @noinspection TypeUnsafeComparisonInspection */
				if (!array_key_exists($k, $b) || ($strict ? $v !== $b[$k] : $v != $b[$k])) {
					return false;
				}
			}
			return true;
		}

		/**
		 * Finds all the missing keys from the given array
		 * @param array $array the array to check
		 * @param string[] $keys an array of keys to search
		 * @return array a sub-array of $keys containing all the keys not found in $array
		 */
		public static function missingKeys(array $array, array $keys) : array {
			$ret = [];
			foreach ($keys as $key) {
				if (!array_key_exists($key, $array)) {
					$ret[] = $key;
				}
			}
			return $ret;
		}

		/**
		 * Merges two array recursively.
		 *
		 * @param array $a the first array
		 * @param array $b the second array
		 * @throws \InvalidArgumentException if two items with the same key cannot be merged
		 * @return array the array obtained by merging the given arrays
		 */
		public static function deepMerge(array $a, array $b) : array {
			$ret = $a;
			foreach ($b as $key => $item) {
				if (!isset($ret[$key])) {
					//Not present or null
					$ret[$key] = $item;
				} elseif ($item !== null) {
					$previous = $ret[$key];
					if ($previous !== $item) {
						if (\is_array($previous) && \is_array($item)) {
							$ret[$key] = self::deepMerge($previous, $item);
						} else {
							throw new \InvalidArgumentException("Cannot merge the items with key '$key': " . \gettype($previous) . ' and ' . \gettype($item));
						}
					}
				}
			}
			return $ret;
		}

		/**
		 * Filters the null values of an array. Keys are preserved
		 * @param array $array the array to filter
		 * @param mixed|null $replacement the value to replace to the null ones. If null, the value will be removed
		 * @return array the filtered array
		 */
		public static function filterNull(array $array, mixed $replacement = null) : array {
			$ret = [];
			foreach ($array as $k => $v) {
				if ($v === null) {
					if ($replacement !== null) {
						$ret[$k] = $replacement;
					}
				} else {
					$ret[$k] = $v;
				}
			}
			return $ret;
		}
	}
