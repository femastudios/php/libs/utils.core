<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	final class StringUtils {

		private function __construct() {
			throw new \LogicException(0);
		}

		/**
		 * @param string $needle the string to search for
		 * @param string $haystack the string to search in
		 * @return bool true if $haystack starts with $needle
		 * @deprecated use PHP str_starts_with
		 */
		public static function startsWith(string $needle, string $haystack) : bool {
			return str_starts_with($haystack, $needle);
		}

		/**
		 * @param string $needle the string to search for
		 * @param string $haystack the string to search in
		 * @return bool true if $haystack ends with $needle
		 * @deprecated use PHP str_ends_with
		 */
		public static function endsWith(string $needle, string $haystack) : bool {
			return str_ends_with($haystack, $needle);
		}

		/**
		 * Reverses a string
		 * @param string $string the string to reverse
		 * @return string the reversed string
		 */
		public static function reverse(string $string) : string {
			$length = mb_strlen($string);
			$reversed = '';
			while ($length-- > 0) {
				$reversed .= mb_substr($string, $length, 1);
			}
			return $reversed;
		}

		/**
		 * Iterates to characters of the given string and finds the number of repeated chars of at least $minChars characters.
		 * @param string $string the string to test
		 * @param int $minChars the minimum number of characters that have to be repeated in order to consider them a block. Defaults to 3
		 * @param bool $ignoreCase weather to ignore case or not
		 * @return int the total number of characters that are in repeated blocks
		 */
		public static function getRepeatedCharsCount(string $string, int $minChars = 3, bool $ignoreCase = false) : int {
			if ($ignoreCase) {
				$string = mb_convert_case($string, MB_CASE_LOWER);
			}
			$strLen = mb_strlen($string);
			if ($strLen < $minChars) {
				return 0;
			} else {
				$c = mb_substr($string, 0, 1);
				$ret = 0;
				$cnt = 1;
				for ($i = 1; $i < $strLen; $i++) {
					$charAt = mb_substr($string, $i, 1);
					if ($c === $charAt) {
						$cnt++;
					} else {
						$c = $charAt;
						if ($cnt >= $minChars) {
							$ret += $cnt;
						}
						$cnt = 1;
					}
				}
				if ($cnt >= $minChars) {
					$ret += $cnt;
				}
				return $ret;
			}
		}

		/**
		 * Performs a replacement
		 * @param string $search the string to search for
		 * @param string $replace the replacement
		 * @param string $subject the string to search in
		 * @param int $count pass-by-reference counter that counts how many replacements have been done. Can be omitted.
		 * @return string the replaced string
		 */
		public static function replace(string $search, string $replace, string $subject, int &$count = 0) : string {
			if ($search === '') {
				$count = 0;
				return $subject;
			}
			$parts = preg_split('/' . preg_quote($search, '/') . '/u', $subject);
			$count = \count($parts) - 1;
			return implode($replace, $parts);
		}

		/**
		 * Normalizes all newlines replacing them with a line-feed (\n)
		 * @param string $str the string
		 * @return string the normalized string
		 */
		public static function normalizeNewlines(string $str) : string {
			return mb_ereg_replace('(\\r\\n|\\r)', "\n", $str);
		}

		/**
		 * Obtains the list of characters contained in the given string, and returns them as an array.
		 * This method can be used with multibyte strings as it uses preg_split in conjunction with the /u option in order to achieve its goal.
		 * @param string $str the string to obtains the char of
		 * @return string[] the array of chars
		 */
		public static function chars(string $str) : array {
			$ret = preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
			if ($ret === false) {
				throw new \LogicException('Illegal state. preg_split errored');
			}
			return $ret;
		}

		private static function firstNonDigitCharIndex(string $str) : int {
			$len = mb_strlen($str);
			for ($i = 0; $i < $len; $i++) {
				if (!ctype_digit(mb_substr($str, $i, 1))) {
					return $i;
				}
			}
			return $len;
		}

		/**
		 * Compares two version string with dotted separated alphanumeric characters. Each dot-separated-string is processed singularly
		 * @param string $vA the first version
		 * @param string $vB the second version
		 * @return int <0 if $vA less than $vB, 0 if $va equals $vB, >0 if $vA greater than $vB
		 */
		public static function versionCompare(string $vA, string $vB) : int {
			$splitA = explode('.', $vA);
			$splitB = explode('.', $vB);

			$splitALen = \count($splitA);
			$splitBLen = \count($splitB);
			for ($i = 0, $max = min($splitALen, $splitBLen); $i < $max; $i++) {
				$groupA = $splitA[$i];
				$groupB = $splitB[$i];
				if ($groupA !== $groupB) {
					$aIndexOf = StringUtils::firstNonDigitCharIndex($groupA);
					$bIndexOf = StringUtils::firstNonDigitCharIndex($groupB);

					$numberPartA = $aIndexOf === 0 ? 0 : (int)mb_substr($groupA, 0, $aIndexOf);
					$stringPartA = mb_substr($groupA, $aIndexOf);

					$numberPartB = $bIndexOf === 0 ? 0 : (int)mb_substr($groupB, 0, $bIndexOf);
					$stringPartB = mb_substr($groupB, $aIndexOf);

					if ($numberPartA === $numberPartB) {
						$cmp = strcasecmp($stringPartA, $stringPartB);
						if ($cmp !== 0) {
							return $cmp;
						}
					} else {
						return MathUtils::compareInt($numberPartA, $numberPartB);
					}
				}
			}
			if ($splitALen === $splitBLen) {
				return 0;
			} else {
				return MathUtils::compareInt($splitALen, $splitBLen);
			}
		}

		public static function parseIntOrNull(string $str) : ?int {
			if (preg_match('/^[+-]?\d+$/', $str) === 1) {
				return (int)$str;
			} else {
				return null;
			}
		}

		public static function parseInt(string $str) : ?int {
			$ret = self::parseIntOrNull($str);
			if ($ret === null) {
				throw new \DomainException("The string '$str' is not a valid int!");
			}
			return $ret;
		}
	}