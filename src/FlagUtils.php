<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;


	final class FlagUtils {

		private function __construct() {
			throw new \LogicException();
		}

		public static function hasFlag(int $flags, int $flag) : bool {
			return ($flags & $flag) === $flag;
		}

		public static function hasOneFlag(int $flags, int $flag) : bool {
			return ($flags & $flag) !== 0;
		}
	}