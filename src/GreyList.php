<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	/**
	 * A class that can act either as a blacklist or as a whitlist
	 * @package com\femastudios\utils\core
	 */
	class GreyList {

		private bool $whitelist;
		private ?array $allowedTypes;
		private bool $allowNull;
		private bool $strict;
		private array $items = [];

		protected function __construct(bool $whitelist, ?array $allowedTypes, bool $allowNull, bool $strict) {
			if ($allowedTypes !== null && \count($allowedTypes) === 0) {
				throw new \DomainException('You must allow at least one type');
			}
			$this->whitelist = $whitelist;
			$this->allowedTypes = $allowedTypes;
			$this->allowNull = $allowNull;
			$this->strict = $strict;
		}

		public function isWhitelist() : bool {
			return $this->whitelist;
		}

		public function isBlacklist() : bool {
			return !$this->whitelist;
		}

		private function become(bool $whitelist, bool $clearItems) : self {
			$this->whitelist = $whitelist;
			if ($clearItems) {
				$this->clear();
			}
			return $this;
		}

		public function becomeBlacklist(bool $clearItems = true) : self {
			return $this->become(false, $clearItems);
		}

		public function becomeWhitelist(bool $clearItems = true) : self {
			return $this->become(true, $clearItems);
		}

		/**
		 * @return string[]|null
		 */
		public function getAllowedTypes() : ?array {
			return $this->allowedTypes;
		}

		public function allowsNull() : bool {
			return $this->allowNull;
		}

		public function isStrict() : bool {
			return $this->strict;
		}

		public function size() : int {
			return \count($this->items);
		}

		public function isEmpty() : bool {
			return $this->size() === 0;
		}

		public function clear() : bool {
			$ret = !$this->isEmpty();
			$this->items = [];
			return $ret;
		}

		public function add(...$items) : self {
			foreach ($items as $k => $item) {
				$this->items[] = ObjectsUtils::requireTypes("items[$k]", $item, $this->allowedTypes ?? [], true, $this->allowNull);
			}
			return $this;
		}

		public function remove($item) : bool {
			$k = \array_search($item, $this->items, $this->strict);
			if ($k === false) {
				return false;
			} else {
				unset($this->items[$k]);
				return true;
			}
		}

		public function contains($item) : bool {
			return \in_array($item, $this->items, $this->strict);
		}

		public function filter(array $allItems) : array {
			return $this->filterFunctional(static function () use ($allItems) {
				return $allItems;
			});
		}

		/**
		 * @param callable $getAll callable to obtain all possible items (used for blacklist)
		 * @return array array of items that are whitelisted/not blacklisted
		 */
		public function filterFunctional(callable $getAll) : array {
			if ($this->isWhitelist()) {
				return $this->items;
			} else {
				$allItems = $getAll();
				foreach ($allItems as $k => $item) {
					if (\in_array($item, $this->items, $this->strict)) {
						unset($allItems[$k]);
					}
				}
				return $allItems;
			}
		}

		/**
		 * Creates a new whitelist
		 * @param string[] $allowedTypes list lof allowed types. You can leave null to allow any type
		 * @param bool $allowNull to allow null in the items
		 * @param bool $strict how to compare the items (when removing and when filtering)
		 * @return GreyList the whitelist
		 */
		public static function newWhitelist(?array $allowedTypes = null, bool $allowNull = false, bool $strict = true) : GreyList {
			return new GreyList(true, $allowedTypes, $allowNull, $strict);
		}

		/**
		 * Creates a new blacklist
		 * @param string[] $allowedTypes list lof allowed types. You can leave null to allow any type
		 * @param bool $allowNull to allow null in the items
		 * @param bool $strict how to compare the items (when removing and when filtering)
		 * @return GreyList the blacklist
		 */
		public static function newBlacklist(?array $allowedTypes = null, bool $allowNull = false, bool $strict = true) : GreyList {
			return new GreyList(false, $allowedTypes, $allowNull, $strict);
		}
	}