<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	/**
	 * Class that contains some helper mathematical function
	 * @package fema\utils
	 */
	final class MathUtils {

		private function __construct() {
			throw new \LogicException();
		}

		/**
		 * Forces the given int value to be in the given interval
		 * @param $value int the value
		 * @param $min int the minimum value
		 * @param $max int the maximum value
		 * @return int $value if $min<=$value<=$max, $max if $value>$max, $min if $value<$min
		 */
		public static function constrainInt(int $value, int $min, int $max) : int {
			return max($min, min($value, $max));
		}

		/**
		 * Forces the given float value to be in the given interval
		 * @param $value float the value
		 * @param $min float the minimum value
		 * @param $max float the maximum value
		 * @return float $value if $min<=$value<=$max, $max if $value>$max, $min if $value<$min
		 */
		public static function constrainFloat(float $value, float $min, float $max) : float {
			return max($min, min($value, $max));
		}

		/**
		 * Compares two ints
		 * @param int $a the first int
		 * @param int $b the second int
		 * @return int 0 if $a==$b, -1 if $a<$b, +1 if $a>$b
		 */
		public static function compareInt(int $a, int $b) : int {
			if ($a < $b) {
				return -1;
			} elseif ($a > $b) {
				return 1;
			} else {
				return 0;
			}
		}

		/**
		 * Compares two floats
		 * @param float $a the first int
		 * @param float $b the second int
		 * @return int 0 if $a==$b, -1 if $a<$b, +1 if $a>$b
		 */
		public static function compareFloat(float $a, float $b) : int {
			if ($a < $b) {
				return -1;
			} elseif ($a > $b) {
				return 1;
			} else {
				return 0;
			}
		}

		public static function optRandomInt(int $min, int $max) : ?int {
			try {
				return random_int($min, $max);
			} catch (\Exception) {
				return null;
			}
		}

		public static function randomInt(int $min, int $max) : int {
			$ret = self::optRandomInt($min, $max);
			/** @noinspection RandomApiMigrationInspection */
            return $ret ?? mt_rand($min, $max);
		}

	}