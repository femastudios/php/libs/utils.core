<?php
	declare(strict_types=1);

	namespace com\femastudios\utils\core;

	/**
	 * Utility methods for {@link Comparable} objects
	 * @package fema\utils
	 */
	final class ComparableUtils {

		private function __construct() {
			throw new \LogicException();
		}

		/**
		 * @param $o1 Comparable the first object to compare
		 * @param $o2 mixed the second object to compare
		 * @return bool true if $o1 is greater than $o2
		 */
		public static function greaterThan(Comparable $o1, mixed $o2) : bool {
			return $o1->compareTo($o2) > 0;
		}

		/**
		 * @param $o1 Comparable the first object to compare
		 * @param $o2 mixed the second object to compare
		 * @return bool true if $o1 is less than $o2
		 */
		public static function lessThan(Comparable $o1, mixed $o2) : bool {
			return $o1->compareTo($o2) < 0;
		}

		/**
		 * @param $o1 Comparable the first object to compare
		 * @param $o2 mixed the second object to compare
		 * @return bool true if $o1 is greater or equals than $o2
		 */
		public static function greaterOrEqualsThan(Comparable $o1, mixed $o2) : bool {
			return $o1->compareTo($o2) >= 0;
		}

		/**
		 * @param $o1 Comparable the first object to compare
		 * @param $o2 mixed the second object to compare
		 * @return bool true if $o1 is less or equals than $o2
		 */
		public static function lessOrEqualsThan(Comparable $o1, $o2) : bool {
			return $o1->compareTo($o2) <= 0;
		}

	}