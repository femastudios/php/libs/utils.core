# utils.core
Core utils library that contains some basic helper classes

# Installation
`composer require femastudios/utils.core`<br>
Requires PHP 7.1 and ext-mbstring enabled.